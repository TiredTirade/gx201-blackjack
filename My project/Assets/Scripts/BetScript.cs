using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetScript : MonoBehaviour
{
    public int firstBetAmount;
    [SerializeField] int maxBet;
    [SerializeField] int minBet;
    [SerializeField] GameObject managerObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FirstBet(int betAmount)
    {
        Player player = managerObject.GetComponent<Player>();
        player.totalBet = 0;
        firstBetAmount = betAmount;
        Wallet wallet = managerObject.GetComponent<Wallet>();
        UIInGameUIManager uIInGameUIManager = GetComponent<UIInGameUIManager>();
        if (BetValidityCheck(betAmount))
        {
            uIInGameUIManager.UpdateBetInputUI("0");
            uIInGameUIManager.SetBetting(false);
            Manager manager = managerObject.GetComponent<Manager>();
            Debug.Log($"Has been dealt first round: {manager.hasDealtFirstRound}");
            manager.InitialDeal();
            House house = managerObject.GetComponent<House>();
            if (house.houseCardValues[0] == 11)
            {
                uIInGameUIManager.ButtonEnabled(3,true);
            }
                else
                {
                    uIInGameUIManager.ButtonEnabled(3,false);
                }
        }
    }
    public void DoubleDownBet()
    {
        Player player = managerObject.GetComponent<Player>();
        PlayerActions playerActions = managerObject.GetComponent<PlayerActions>();
        if (BetValidityCheck(firstBetAmount))
        {
            player.totalBet = 2*firstBetAmount;
            playerActions.Hit();
            playerActions.Stand();
        }
    }
    public void InsuranceBet()
    {
        if (BetValidityCheck(firstBetAmount/2))
        {
            Player player = managerObject.GetComponent<Player>();
            player.insuranceBet = firstBetAmount/2;
        }
    }
    private bool BetValidityCheck(int betAmount)
    {
        Player player = managerObject.GetComponent<Player>();
        Wallet wallet = managerObject.GetComponent<Wallet>();
        UIInGameUIManager uIInGameUIManager = GetComponent<UIInGameUIManager>();
        if (betAmount <= maxBet && betAmount >= minBet && betAmount <= wallet.playerWalletTotal)
        {
            wallet.UpdatePlayerWalletTotal(-betAmount);
            player.totalBet =+ betAmount;
            // return totalBet;
            Debug.Log($"Total bet is: {player.totalBet}");
            return true;
        }
            else if (betAmount !<= maxBet || betAmount !>= minBet)
            {
                Debug.Log("Betting outside of min and max!");
                uIInGameUIManager.ErrorDisplayMessage("Betting outside of min and max!");
                return false;

            }
            else if (betAmount !<= wallet.playerWalletTotal)
            {
                Debug.Log("Insufficient funds!");
                uIInGameUIManager.ErrorDisplayMessage("Insufficient funds!");
                return false;
            }
                else
                {
                Debug.Log("Unknown error");
                uIInGameUIManager.ErrorDisplayMessage("Unknown error");
                return false;
                }
    }
}

