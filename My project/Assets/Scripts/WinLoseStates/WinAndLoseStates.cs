using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinAndLoseStates : MonoBehaviour
{
    private Player player;
    private House house;
    private Wallet wallet;
    [SerializeField] UIInGameUIManager uIInGameUIManager;
    // Start is called before the first frame update
    void Start()
    {
        // uIInGameUIManager = GetComponent<UIInGameUIManager>();
        player = GetComponent<Player>();
        house = GetComponent<House>();
        wallet = GetComponent<Wallet>();
    }

    public void CompareHands()
    {
        if (player.playerTotal < house.houseTotal)
        {
            PlayerBusts();
        }
        else if (player.playerTotal > house.houseTotal)
        {
            HouseBusts();
        }
        else if (player.playerTotal == house.houseTotal)
        {
            Push();
        }
    }

    public void Push()
    {
        wallet.playerWalletTotal = wallet.playerWalletTotal + player.totalBet;
        uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
        Debug.Log($"Tie. Player gets their bet of {player.totalBet} back. Total is: {wallet.playerWalletTotal}");
        uIInGameUIManager.ShowWinLoseStateDisplay($"Tie. Player gets their bet of {player.totalBet} back.");
    }

    public void PlayerBlackjackWin()
    {
        wallet.playerWalletTotal = 3*player.totalBet + wallet.playerWalletTotal;
        uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
        Debug.Log($"Player wins. Gets three times bet: {player.totalBet}. Which is a total of: {3*player.totalBet}. Wallet total is now: {wallet.playerWalletTotal}.");
        uIInGameUIManager.ShowWinLoseStateDisplay($"Player has blackjack. Gets three times bet: {player.totalBet}. Which is a total of: {3*player.totalBet}.");
    }

    public void HouseBlackjackWin()
    {
        if (player.insuranceBet > 0)
        {
            wallet.playerWalletTotal = 2*player.insuranceBet + wallet.playerWalletTotal;
            uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
            Debug.Log($"Player loses. Gets double insurance bet: {player.insuranceBet}. Loses original bet of {player.totalBet}. Total is this: {wallet.playerWalletTotal}");
            uIInGameUIManager.ShowWinLoseStateDisplay($"House has blackjack. Gets double insurance bet: {player.insuranceBet}. Loses original bet of {player.totalBet}.");
        }
            else
            {
                Debug.Log($"House wins. Player loses: {player.totalBet} AND {player.insuranceBet}. Total is: {wallet.playerWalletTotal}");
                uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
                uIInGameUIManager.ShowWinLoseStateDisplay($"House has blackjack. Player loses: {player.totalBet} AND {player.insuranceBet}.");   
            }
    }

    public void PlayerBusts()
    {
        //wallet.playerWalletTotal = wallet.playerWalletTotal - player.totalBet;
        Debug.Log($"Player loses {player.totalBet}. Wallet total now: {wallet.playerWalletTotal}");
        uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
        uIInGameUIManager.ShowWinLoseStateDisplay($"Player busts. Player loses {player.totalBet}.");
    }

    public void HouseBusts()
    {
        wallet.playerWalletTotal = 2*player.totalBet + wallet.playerWalletTotal;
        uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
        Debug.Log($"Player wins. Gets twice the bet: {player.totalBet}. A total of: {2*player.totalBet}. Wallet total is now: {wallet.playerWalletTotal}");
        uIInGameUIManager.ShowWinLoseStateDisplay($"House busts. Player gets twice the bet: {player.totalBet}. A total of: {2*player.totalBet}.");
    }
    
    public void PlayerSurrenders()
    {
        wallet.playerWalletTotal = wallet.playerWalletTotal + player.totalBet/2;
        uIInGameUIManager.UpdatePlayerWallet(wallet.playerWalletTotal);
        Debug.Log($"Player surrenders. Gets half back b bet: {player.totalBet}. Which is a total of: {player.totalBet/2}. Wallet total is now: {wallet.playerWalletTotal}.");
        uIInGameUIManager.ShowWinLoseStateDisplay($"Player surrenders. Gets half back b bet: {player.totalBet}. Which is a total of: {player.totalBet/2}.");
    }
}
