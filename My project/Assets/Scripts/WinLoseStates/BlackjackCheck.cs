using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackjackCheck : MonoBehaviour
{
    private bool houseHasBlackjack = false;
    private bool playerHasBlackjack = false;

    public void CheckBlackjack(int currentTotal, bool isPlayer)
    {
        if (currentTotal == 21)
        {
            if (isPlayer)
            {
                // PlayerBlackjack();
                playerHasBlackjack = true;
                House house = GetComponent<House>();
                CheckBlackjack(house.houseTotal, false);
            }
            if (!isPlayer)
            {
                houseHasBlackjack = true;
                // HouseBlackjack();
            }
        }

        WinAndLoseStates winAndLoseStates = GetComponent<WinAndLoseStates>();
        if (playerHasBlackjack && houseHasBlackjack)
        {
            GetComponent<CardDeck>().BackCardVisible(false);
            winAndLoseStates.Push();
        }
            else if (playerHasBlackjack || houseHasBlackjack)
            {
                if (playerHasBlackjack)
                {
                    winAndLoseStates.PlayerBlackjackWin();
                }
                if (houseHasBlackjack)
                {
                    winAndLoseStates.HouseBlackjackWin();
                }
            }
    }

    private void PlayerBlackjack()
    {
        House house = GetComponent<House>();
        CheckBlackjack(house.houseTotal, false);
    }

    private void HouseBlackjack()
    {

    }
}
