using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    [Header("ScriptRefs")]
    [SerializeField] CardDeck cardDeck;
    [SerializeField] Wallet wallet;
    [SerializeField] Player player;
    [SerializeField] House house;
    [Header("Screens")]
    [SerializeField] GameObject uiScreen;
    [SerializeField] GameObject cardScreen;
    [SerializeField] GameObject startupScreen;
    public bool hasDealtFirstRound;

    // [Header("Conditions")]
    // private bool insurance = false;
    void Start()
    {
        NewGame();
        uiScreen.SetActive(false);
        cardScreen.SetActive(false);
        startupScreen.SetActive(true);
    }

    public void NewGame()
    {
        wallet.CalculatePlayerTotal();
        cardDeck.PopulateDeck();
        NextRound();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void NextRound()
    {
        cardDeck.ClearTableOfCards();
        uiScreen.SetActive(true);
        cardScreen.SetActive(true);
        startupScreen.SetActive(false);

        hasDealtFirstRound = false;

        player.ClearAll();
        house.ClearAll();

        startupScreen.SetActive(false);
        uiScreen.SetActive(true);
        UIInGameUIManager uIInGameUIManager = uiScreen.GetComponent<UIInGameUIManager>();
        uIInGameUIManager.enabled = true;
        uIInGameUIManager.RoundStart();
    }

    public void InitialDeal()
    {
        Debug.Log("Initial deal called");
        for (int i = 1; i <= 2; i++)
        {
            //Deal for Player
            player.DealPlayerCard();

            //Deal for House
            house.DealHouseCard();
        }
        BlackjackCheck blackjack = GetComponent<BlackjackCheck>();
        blackjack.CheckBlackjack(player.playerTotal,true);
        UIInGameUIManager uIInGameUIManager = uiScreen.GetComponent<UIInGameUIManager>();
        uIInGameUIManager.SetBetting(false);
        uIInGameUIManager.DisableAllPlayerButtons(true);
        hasDealtFirstRound = true;
        if (house.houseCardValues[0] == 11)
        {
            uIInGameUIManager.SetInsuranceActive(true);
        }
            else
            {
                uIInGameUIManager.SetInsuranceActive(false);
            }
    }
}
