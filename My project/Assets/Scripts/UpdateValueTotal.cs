using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateValueTotal : MonoBehaviour
{
    [SerializeField] GameObject uIObject;
    public int DetermineTotal(List<int> valueList, bool isPlayer)
    {

        for (int i = 0; i < valueList.Count; i++)
        {
            if (valueList[i] == 11)
            {
                int currentTotal = 0;
                for (int k = 0; k < valueList.Count; k++)
                {
                    currentTotal = currentTotal + valueList[k];
                }
                if (currentTotal > 21)
                {
                    valueList[i] = 1;
                }
            }
        }

        int valueTotal = 0;
        for (int i = 0; i < valueList.Count; i++)
        {
            valueTotal = valueTotal + valueList[i];
        }

        
       DisplayTotal(valueTotal, isPlayer);

        return valueTotal;
    }

    public void DisplayTotal(int valueTotal, bool isPlayer)
    {
        UIInGameUIManager uIInGameUIManager = uIObject.GetComponent<UIInGameUIManager>();
        uIInGameUIManager.SetTotalText(isPlayer, valueTotal);
    }
}
