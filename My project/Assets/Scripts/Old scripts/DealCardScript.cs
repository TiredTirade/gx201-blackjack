using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealCardScript : MonoBehaviour
{
    // Start is called before the first frame update
    private Manager manager;
    private GameObject currentCard;
    private GameObject instantiatedCard;
    [SerializeField] GameObject cardBack;
    private Vector2 cardPosition;
    [SerializeField] float cardWidth;
    void Start()
    {
        Manager manager = GetComponent<Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject DealCard(bool isHouse, int cardCount, Vector2 positionChange, GameObject cardParent)
    {

        CardDeck cardDeck = manager.GetComponent<CardDeck>();
        // GameObject currentCard = cardDeck.DealCard();

        Vector2 cardPosition = new Vector2(((cardCount-1)*cardWidth)+-positionChange.x,positionChange.y);
        GameObject instantiatedCard = Instantiate(currentCard,cardPosition,Quaternion.identity);
        instantiatedCard.transform.SetParent(cardParent.transform, false);

        if (cardCount ==2 &&  isHouse)
        {
            backCardVisible(true);
            PositionBackCard(cardPosition);
        }

        //UpdateValueTotal updateValueTotal = GetComponent<UpdateValueTotal>(currentCard);

        return currentCard;
    }

    private void backCardVisible(bool isVisible)
    {
        cardBack.SetActive(isVisible);
    }
    private void PositionBackCard(Vector2 cardPosition)
    {
        cardBack.transform.localPosition = new Vector2 (cardPosition.x, cardPosition.y);
        cardBack.transform.SetAsLastSibling();
    }
}
