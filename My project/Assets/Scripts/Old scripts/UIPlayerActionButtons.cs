using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerActionButtons : MonoBehaviour
{
    [SerializeField] static Button betButton;
    [SerializeField] static Button standButton;
    [SerializeField] static Button hitButton;
    [SerializeField] static Button splitButton;
    [SerializeField] static Button doubleDownButton;
    [SerializeField] static Button insuranceButton;
    [SerializeField] static Button SurrenderButton;
    private List<Button> playerActionButtons = new List<Button>{betButton,standButton,hitButton,splitButton,doubleDownButton,insuranceButton,SurrenderButton};
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // public void DisableButton(Button button)
    // {
    //     button.enabled = false;
    // }
    
    // public void EnableButton(Button button)
    // {
    //     button.enabled = true;
    // }

    public void DisableAllPlayerButtons()
    {
        int playerActionButtonCount = playerActionButtons.Count;

        for (int i = 0; i <= playerActionButtonCount; i++)
        {
            playerActionButtons[i].enabled = false;
        }
    }
}
