using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIBetUpdate : MonoBehaviour
{
    [SerializeField] TMP_InputField betInput;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateBetInputUI (string betAmount)
    {
        betInput.text = betAmount;
    }
}
