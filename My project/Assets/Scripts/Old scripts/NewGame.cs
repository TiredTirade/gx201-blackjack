using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGame : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PrepareNewGame()
    {
        Player player = GetComponent<Player>();
        House house = GetComponent<House>();
        player.ClearAll();
        house.ClearAll();

        CardDeck cardDeck = GetComponent<CardDeck>();
        cardDeck.PopulateDeck();

        // Wallet wallet = GetComponent<Wallet>();
    }
}
