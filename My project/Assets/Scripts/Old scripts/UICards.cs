using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UICards : MonoBehaviour
{
    [SerializeField] static GameObject cardBack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetBackCard(bool visible)
    {
        
    }

    public void PlaceCard(GameObject cardBeingPlaced, Vector2 cardPosition)
    {
        GameObject instantiatedCard = Instantiate(cardBeingPlaced,cardPosition,Quaternion.identity);
    }
}
