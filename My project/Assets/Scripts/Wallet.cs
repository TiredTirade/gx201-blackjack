using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallet : MonoBehaviour
{
    public int playerWalletTotal;
    [SerializeField] GameObject uIObject;
    [Header("Chips")]
    [SerializeField] GameObject tenDollarChip;
    [SerializeField] GameObject twentyDollarChip;
    [SerializeField] GameObject fiftyDollarChip;
    [SerializeField] GameObject hundredDollarChip;
    [Header("Amount of player chips")]
    [SerializeField] int player10Chips = 2;
    [SerializeField] int player20Chips = 2;
    [SerializeField] int player50Chips = 2;
    [SerializeField] int player100Chips = 2;

    void Start()
    {
        // CardDeck cardDeck = GetComponent<CardDeck>();
        CalculatePlayerTotal();
    }

    public void CalculatePlayerTotal()
    {
        playerWalletTotal = 0;
        playerWalletTotal = (player10Chips * 10) + (player20Chips * 20) + (player50Chips * 50) + (player100Chips * 100);
        uIObject.GetComponent<UIInGameUIManager>().UpdatePlayerWallet(playerWalletTotal);
    }

    public void UpdatePlayerWalletTotal(int amountLostOrGained)
    {
        playerWalletTotal = playerWalletTotal + amountLostOrGained;
        uIObject.GetComponent<UIInGameUIManager>().UpdatePlayerWallet(playerWalletTotal);
    }
}
