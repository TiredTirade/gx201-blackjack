using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardValues : MonoBehaviour
{
    public string cardType;
    public int cardValue;
    public string fullCardName;
}
