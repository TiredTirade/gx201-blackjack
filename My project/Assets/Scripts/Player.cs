using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int totalBet;
    public int insuranceBet;
    private int playerCardCount;
    [SerializeField] GameObject playerCards;
    [SerializeField] Manager manager;
    [SerializeField] Vector2 playerCardPositionChange;
    public List<int> playerCardValues = new List<int>();
    public int playerTotal = 0;
    void Start()
    {
        ClearAll();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearAll()
    {
        totalBet = 0;
        playerTotal = 0;
        playerCardCount = 0;
        playerCardValues.Clear();
    }

    public void DealPlayerCard()
    {
        playerCardCount++;
        CardDeck cardDeck = manager.GetComponent<CardDeck>();
        GameObject currentPlayerCard = cardDeck.DealCard(false, playerCardCount, playerCardPositionChange, playerCards);

        CardValues cardValues = currentPlayerCard.GetComponent<CardValues>();
        playerCardValues.Add(cardValues.cardValue);
        UpdateValueTotal updateValueTotal = GetComponent<UpdateValueTotal>();
        // UpdateValueTotal updateValueTotal = new UpdateValueTotal();
        playerTotal = updateValueTotal.DetermineTotal(playerCardValues, true);

        if (playerTotal > 21)
        {
            WinAndLoseStates winAndLoseStates = GetComponent<WinAndLoseStates>();
            winAndLoseStates.PlayerBusts();
        }
    }
}
