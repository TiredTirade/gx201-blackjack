using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDeck : MonoBehaviour
{
    [SerializeField] float cardWidth;
    public List<GameObject> fullPackOfCards = new List<GameObject>();
    public List<GameObject> tempPackOfCards = new List<GameObject>();
    [SerializeField] GameObject cardBack;
    [SerializeField] GameObject houseCardParent;
    [SerializeField] GameObject playerCardParent;

    // Start is called before the first frame update
    void Start()
    {
        PopulateDeck();
    }

    // Update is called once per frame

    public void PopulateDeck()
    {
        tempPackOfCards.Clear();
        for (int i = 0; i < fullPackOfCards.Count; i++)
        {
            tempPackOfCards.Add(fullPackOfCards[i]);
        }
    }

    public GameObject DealCard(bool isHouse, int cardCount, Vector2 positionChange, GameObject cardParent)
    {
        if (tempPackOfCards.Count <= 0)
        {
            PopulateDeck();
        }

        int randomCardNumber = Random.Range(0,tempPackOfCards.Count);
        GameObject currentCard = tempPackOfCards[randomCardNumber];
        tempPackOfCards.RemoveAt(randomCardNumber);
        Debug.Log($"The card dealt is: {currentCard}");
        
        Vector2 cardPosition = new Vector2(((cardCount-1)*cardWidth)+-positionChange.x,positionChange.y);
        GameObject instantiatedCard = Instantiate(currentCard,cardPosition,Quaternion.identity);
        instantiatedCard.transform.SetParent(cardParent.transform, false);

        if (isHouse && cardCount == 2)
        {
            BackCardVisible(true);
            PositionBackCard(cardPosition);
        }

        return currentCard;
    }
    public void BackCardVisible(bool isVisible)
    {
        cardBack.SetActive(isVisible);
    }
    private void PositionBackCard(Vector2 cardPosition)
    {
        cardBack.transform.localPosition = new Vector2 (cardPosition.x, cardPosition.y);
        cardBack.transform.SetAsLastSibling();
    }

    public void ClearTableOfCards()
    {

        if (houseCardParent.transform.childCount > 0)
        {
            for (int i = 0; i < houseCardParent.transform.childCount; i++)
            {
                Debug.Log($"Destroyed is {houseCardParent.transform.GetChild(i).gameObject}");
                Destroy(houseCardParent.transform.GetChild(i).gameObject);
            }
        }
        if (playerCardParent.transform.childCount > 0)
        {
            for (int i = 0; i < playerCardParent.transform.childCount; i++)
            {
                Debug.Log($"Destroyed is {playerCardParent.transform.GetChild(i).gameObject}");
                Destroy(playerCardParent.transform.GetChild(i).gameObject);
            }
        }
        cardBack.SetActive(false);
    }
}
