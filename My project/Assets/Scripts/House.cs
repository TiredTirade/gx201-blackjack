using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    [SerializeField] GameObject houseCards;
    public GameObject cardBack;
    [SerializeField] Manager manager;
    private int houseCardCount;
    public List<int> houseCardValues = new List<int>();
    public int houseTotal = 0;
    [SerializeField] Vector2 houseCardPositionChange;
    // Start is called before the first frame update
    void Start()
    {
        ClearAll();
    }

    public void ClearAll()
    {
        cardBack.SetActive(false);
        houseCardCount = 0;
        houseCardValues.Clear();
    }

    public void DealHouseCard()
    {
        houseCardCount++;
        CardDeck cardDeck = GetComponent<CardDeck>();
        GameObject currentHouseCard = cardDeck.DealCard(true, houseCardCount, houseCardPositionChange, houseCards);

        CardValues cardValues = currentHouseCard.GetComponent<CardValues>();
        houseCardValues.Add(cardValues.cardValue);
        UpdateValueTotal updateValueTotal = GetComponent<UpdateValueTotal>();
        houseTotal = updateValueTotal.DetermineTotal(houseCardValues, false);

        if (houseCardCount == 2)
        {
            
            // houseTotal = houseCardValues[0];
            updateValueTotal.DisplayTotal(houseCardValues[0], false);
        }

        if (houseTotal > 21)
        {
            WinAndLoseStates winAndLoseStates = GetComponent<WinAndLoseStates>();
            winAndLoseStates.HouseBusts();
        }

        Debug.Log($"House total is: {houseTotal}");
    }

    public void HouseTurnStart()
    {
        CardDeck cardDeck = GetComponent<CardDeck>();
        cardDeck.BackCardVisible(false);
        UpdateValueTotal updateValueTotal = GetComponent<UpdateValueTotal>();
        houseTotal = updateValueTotal.DetermineTotal(houseCardValues, false);

        BlackjackCheck blackjackCheck = GetComponent<BlackjackCheck>();
        blackjackCheck.CheckBlackjack(houseTotal,false);

        HousePlay();
    }

    private void HousePlay()
    {
        while (houseTotal < 17)
        {
            DealHouseCard();
        }

        WinAndLoseStates winAndLoseStates = GetComponent<WinAndLoseStates>();
        if (houseTotal > 21)
        {
            winAndLoseStates.HouseBusts();
        }
        if (houseTotal >= 17 && houseTotal <= 21)
        {
            winAndLoseStates.CompareHands();
        }
    }
}
