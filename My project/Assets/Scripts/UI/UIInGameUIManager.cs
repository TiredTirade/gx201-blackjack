using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIInGameUIManager : MonoBehaviour
{
    [SerializeField] GameObject screenRef;
    // [SerializeField] GameObject managerGameObject;
    [SerializeField] GameObject betPanel;
    [SerializeField] GameObject playerActionsPanel;
    [SerializeField] Button betButton;
    [SerializeField] Button standButton;
    [SerializeField] Button hitButton;
    // [SerializeField] Button splitButton;
    [SerializeField] Button doubleDownButton;
    [SerializeField] Button insuranceButton;
    [SerializeField] Button surrenderButton;
    [SerializeField] TMP_Text playerTotalText;
    [SerializeField] TMP_Text houseTotalText;
    [SerializeField] TMP_Text walletTotalDisplay;
    [SerializeField] GameObject errorDisplay;
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_InputField betInput;

    [SerializeField] GameObject pauseMenuDisplay;
    [SerializeField] GameObject startUpMenuDisplay;
    [SerializeField] GameObject winLoseStateDisplay;
    [SerializeField] TMP_Text winLoseStateText;
    private List<Button> playerActionButtons = new List<Button>();
    void Start()
    {
        List<Button> playerActionButtons = new List<Button>{hitButton,standButton,doubleDownButton,insuranceButton,surrenderButton};
    }

    public void RoundStart()
    {
        DisableAllPlayerButtons(false);
        betInput.text = null;
        SetDoubleDownActive(true);
        //SetBetting(true);
        SetTotalText(true, 0);
        SetTotalText(false, 0);
        errorDisplay.SetActive(false);
        pauseMenuDisplay.SetActive(false);
        winLoseStateDisplay.SetActive(false);

        SetBetting(true);
    }

    public void ButtonEnabled (int buttonNumber, bool isActive)
    {
        playerActionButtons[buttonNumber].enabled = isActive;
    }

    public void SetInsuranceActive(bool isActive)
    {
        insuranceButton.enabled = isActive;
    }
    public void SetDoubleDownActive(bool isActive)
    {
        doubleDownButton.enabled = isActive;
    }

    public void DisableAllPlayerButtons(bool isVisible)
    {
        playerActionsPanel.SetActive(isVisible);
    }

    public void SetBetting(bool enabled)
    {
        betPanel.SetActive(enabled);
    }

    public void SetTotalText(bool isPlayer, int totalValue)
    {
        string whichBeingSet = null;
        TMP_Text textToSet = null;
        switch (isPlayer)
        {
            case true: 
                whichBeingSet = "Player";
                textToSet = playerTotalText;
                break;
            case false:
                whichBeingSet = "House";
                textToSet = houseTotalText;
                break;
        }

        textToSet.text = whichBeingSet + " total: " +  totalValue.ToString();
    }
    public void PlayerBet()
    {
        int betAmount;
        betAmount = int.Parse(betInput.text);
        Debug.Log($"Current bet: {betAmount}");

        BetScript betScript = GetComponent<BetScript>();
        betScript.FirstBet(betAmount);
    }

    public void UpdatePlayerWallet(int walletTotal)
    {
        walletTotalDisplay.text = "Total wallet: " + walletTotal.ToString();
    }

    public void ErrorDisplayMessage(string messageDisplayed)
    {
        // screenRef.
        errorDisplay.SetActive(true);
        errorText.text = messageDisplayed;
    }
    public void CloseErrorDisplayMessage()
    {
        errorDisplay.SetActive(false);
    }
    public void UpdateBetInputUI(string betAmount)
    {
        betInput.text = betAmount;
    }

    public void ShowPauseMenu()
    {
        pauseMenuDisplay.SetActive(true);
    }

    public void HideMenus()
    {
        pauseMenuDisplay.SetActive(false);
        winLoseStateDisplay.SetActive(false);
    }
    
    public void ShowMainMenu()
    {
        pauseMenuDisplay.SetActive(false);
        errorDisplay.SetActive(false);
        startUpMenuDisplay.SetActive(true);
    }

    public void ShowWinLoseStateDisplay(string messageShown)
    {
        winLoseStateDisplay.SetActive(true);
        winLoseStateText.text = messageShown;
    }
}
