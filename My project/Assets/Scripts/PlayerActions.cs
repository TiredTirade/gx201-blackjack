using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    [SerializeField] GameObject uIManagerObject;
    private BetScript betScript;
    private UIInGameUIManager uIInGameUIManager;
    private Player player;
    private House house;
    private WinAndLoseStates winAndLoseStates;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Player>();
        house = GetComponent<House>();
        winAndLoseStates = GetComponent<WinAndLoseStates>();
        uIInGameUIManager = uIManagerObject.GetComponent<UIInGameUIManager>();
        betScript = uIManagerObject.GetComponent<BetScript>();
    }

    public void Hit()
    {
        player.DealPlayerCard();
        uIInGameUIManager.SetDoubleDownActive(false);
    }

    public void Stand()
    {
        if (player.playerTotal <= 21)
        {
        house.HouseTurnStart();
        uIInGameUIManager.DisableAllPlayerButtons(false);
        }
        else 
        {
            winAndLoseStates.PlayerBusts();
        }
    }
    public void DoubleDown()
    {
        betScript.DoubleDownBet();
        uIInGameUIManager.SetDoubleDownActive(false);
    }
    public void Insurance()
    {
        betScript.InsuranceBet();
        uIInGameUIManager.ButtonEnabled(3,true);
        uIInGameUIManager.SetDoubleDownActive(false);
    }
    public void Surrender()
    {
        uIInGameUIManager.DisableAllPlayerButtons(false);
        WinAndLoseStates winAndLoseStates = GetComponent <WinAndLoseStates>();
        winAndLoseStates.PlayerSurrenders();
    }
}
